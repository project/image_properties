/**
 * @file
 * Image Properties jQuery MiniColors behaviors.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Displays jQuery MiniColors on designated fields.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   */
  Drupal.behaviors.imagePropertiesjQueryMiniColors = {
    attach: function (context) {
      $(context).find('[data-color-picker]').minicolors([]);
    }
  };

})(jQuery, Drupal);
