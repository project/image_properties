<?php

namespace Drupal\image_properties;

use Drupal\Core\Image\ImageInterface;

/**
 * Implementation of the image processing service.
 *
 * @see \Drupal\image_properties\ImageProcessingInterface
 */
class ImageProcessing implements ImageProcessingInterface {

  /**
   * {@inheritdoc}
   */
  public function computeAverageImageColor(ImageInterface $image) {

    // Only GD is supported by now.
    if ($image->getToolkitId() != 'gd') {
      \Drupal::logger('image_properties')->warning("Unsupported image toolkit '{$image->getToolkitId()}' for average image color computation, currently only GD is supported.");
      return NULL;
    }

    /** @var \Drupal\system\Plugin\ImageToolkit\GDToolkit $toolkit */
    $toolkit = $image->getToolkit();
    $res = $toolkit->getResource();

    // Resample image to 1 x 1 px and sample the single remaining pixel.
    // @see https://stackoverflow.com/questions/6962814/average-of-rgb-color-of-image
    $pixel = imagecreatetruecolor(1, 1);
    imagecopyresampled($pixel, $res, 0, 0, 0, 0, 1, 1, $toolkit->getWidth(), $toolkit->getHeight());
    $color = imagecolorat($pixel, 0, 0);

    // Convert color value.
    $rgb = [
      // Red.
      ($color >> 16) & 0xFF,
      // Green.
      ($color >> 8) & 0xFF,
      // Blue.
      ($color >> 0) & 0xFF,
    ];

    return $this->rgbToHex($rgb);
  }

  /**
   * {@inheritdoc}
   */
  public function computeColorLuminosity($color) {
    $rgb = $this->hexToRgb($color);
    $luminosity = ((float) (($rgb[0] + $rgb[1] + $rgb[2]) / 3)) / 0xFF;
    return $luminosity;
  }

  /**
   * {@inheritdoc}
   */
  public function hexToRgb($hex) {
    $hex = str_replace('#', '', $hex);
    $r = hexdec(substr($hex, 0, 2));
    $g = hexdec(substr($hex, 2, 2));
    $b = hexdec(substr($hex, 4, 2));
    return [$r, $g, $b];
  }

  /**
   * {@inheritdoc}
   */
  public function rgbToHex(array $rgb) {
    $hex  = '#';
    $hex .= str_pad(dechex($rgb[0]), 2, '0', STR_PAD_LEFT);
    $hex .= str_pad(dechex($rgb[1]), 2, '0', STR_PAD_LEFT);
    $hex .= str_pad(dechex($rgb[2]), 2, '0', STR_PAD_LEFT);
    return $hex;
  }

}
