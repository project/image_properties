<?php

namespace Drupal\image_properties\Plugin;

use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\image_properties\Plugin\Field\FieldType\ImagePropertiesItemInterface;

/**
 * Interface for image property plugins.
 */
interface ImagePropertyInterface extends PluginInspectionInterface, DerivativeInspectionInterface {

  /**
   * Retrieve the plugin provider.
   *
   * @return string
   *   The provider name,
   */
  public function getProvider();

  /**
   * Retrieve the plugin label.
   *
   * @return string
   *   The plugin label.
   */
  public function getLabel();

  /**
   * Retrieve the plugin description.
   *
   * @return string
   *   The plugin description.
   */
  public function getDescription();

  /**
   * Retrieve the property storage schema.
   *
   * @return array
   *   An array describing the property schema, same format as
   *   \Drupal\Core\Field\FieldItemInterface::schema() returns.
   *
   * @see \Drupal\Core\Field\FieldItemInterface::schema()
   */
  public function schema();

  /**
   * Defines field item properties.
   *
   * @param FieldStorageDefinitionInterface $field_definition
   *   The field storage definition.
   *
   * @return \Drupal\Core\TypedData\DataDefinitionInterface[]
   *   An array of property definitions of contained properties, keyed by
   *   property name.
   *
   * @see \Drupal\Core\Field\FieldItemInterface::propertyDefinitions()
   */
  public function propertyDefinitions(FieldStorageDefinitionInterface $field_definition);

  /**
   * Retrieve the image properties this property depends on.
   *
   * @return array
   *   An array of image property plugin IDs which this plugin depends on.
   */
  public function getPropertyDependencies();

  /**
   * Create a form element for this property.
   *
   * @param \Drupal\image_properties\Plugin\Field\FieldType\ImagePropertiesItemInterface $item
   *   The field item.
   * @param int $delta
   *   The item delta.
   * @param array $element
   *   The widget form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state of the containing form.
   *
   * @return array
   *   The modified widget form element.
   */
  public function formElement(ImagePropertiesItemInterface $item, $delta, array &$element, FormStateInterface $form_state);

  /**
   * Handle form submit.
   *
   * @param array $values
   *   Array of values extracted from the form, keyed by delta.
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state of the containing form.
   *
   * @return array
   *   The massaged values array, keyed by delta.
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state);

  /**
   * Auto-compute the value.
   *
   * @param \Drupal\image_properties\Plugin\Field\FieldType\ImagePropertiesItemInterface $item
   *   The field item instance.
   * @param array $values
   *   The current field item value.
   * @param \Drupal\Core\Image\ImageInterface $image
   *   The provided image.
   *
   * @return array
   *   An array containing the computed value properties, keyed by property
   *   value id.
   *
   * @see static::propertyDefinitions()
   */
  public function computeValue(ImagePropertiesItemInterface $item, array $values, ImageInterface $image);

}
