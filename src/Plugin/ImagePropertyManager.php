<?php

namespace Drupal\image_properties\Plugin;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultLazyPluginCollection;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Implementation of the image property plugin manager.
 */
class ImagePropertyManager extends DefaultPluginManager implements ImagePropertyManagerInterface {

  /**
   * The plugin collection.
   *
   * @var \Drupal\Component\Plugin\LazyPluginCollection
   */
  protected $allPlugins;

  /**
   * ImagePropertyManager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/ImageProperties/Property',
      $namespaces,
      $module_handler,
      'Drupal\image_properties\Plugin\ImagePropertyInterface',
      'Drupal\image_properties\Annotation\ImageProperty');
  }

  /**
   * {@inheritdoc}
   */
  public function getAll() {
    if (!$this->allPlugins) {
      $collection = new DefaultLazyPluginCollection($this, []);
      foreach ($this->getDefinitions() as $plugin_id => $plugin_info) {
        $collection->setInstanceConfiguration($plugin_id, ['id' => $plugin_id]);
      }
      $this->allPlugins = $collection->sort();
    }
    return $this->allPlugins;
  }

}
