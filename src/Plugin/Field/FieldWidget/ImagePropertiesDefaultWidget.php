<?php

namespace Drupal\image_properties\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Element;
use Drupal\image_properties\Plugin\ImagePropertyManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'image_properties_default' widget.
 *
 * @FieldWidget(
 *   id = "image_properties_default",
 *   label = @Translation("Image properties"),
 *   field_types = {
 *     "image_properties"
 *   },
 * )
 */
class ImagePropertiesDefaultWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The image property manager.
   *
   * @var ImagePropertyManagerInterface
   */
  protected $propertyManager;

  /**
   * ImagePropertiesDefaultWidget constructor.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\image_properties\Plugin\ImagePropertyManagerInterface $property_manager
   *   The image property plugin manager.
   */
  public function __construct($plugin_id,
                              $plugin_definition,
                              FieldDefinitionInterface $field_definition,
                              array $settings,
                              array $third_party_settings,
                              ImagePropertyManagerInterface $property_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->propertyManager = $property_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('plugin.manager.image_property')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'jquery_minicolors' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $element['jquery_minicolors'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('User jQuery MiniColors'),
      '#default_value' => $this->getSetting('jquery_minicolors'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary['jquery_minicolors'] = $this->t('Use jQuery MiniColors: @yesno', ['@yesno' => $this->getSetting('jquery_minicolors') ? 'Yes' : 'No']);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $jquery_minicolors = $this->getSetting('jquery_minicolors');

    /** @var \Drupal\image_properties\Plugin\Field\FieldType\ImagePropertiesItemInterface $item */
    $item = $items[$delta];

    $element += [
      '#type' => 'details',
      '#title' => $this->t('Image properties'),
      '#collapsible' => TRUE,
      '#open' => TRUE,
    ];

    // Allow all image properties to modify the form.
    /** @var \Drupal\image_properties\Plugin\ImagePropertyInterface $property */
    foreach ($this->propertyManager->getAll() as $property) {
      $element = $property->formElement($item, $delta, $element, $form_state);
    }

    // Add the option to change the value manually (if configured so).
    $compute_field_name = "{$items->getFieldDefinition()->getName()}[{$delta}][compute]";
    foreach (Element::children($element) as $child) {
      $element[$child]['#states']['disabled'] = [
        'input[name="' . $compute_field_name . '"]' => ['checked' => TRUE],
      ];
    }
    $element['compute'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatic'),
      '#default_value' => $item->compute,
      '#weight' => -1,
    ];

    // Swap 'color' field for jQuery MiniColors.
    // @todo Integrate with the jquery_minicolors module
    if ($jquery_minicolors) {
      foreach (Element::children($element) as $child) {
        if ($element[$child]['#type'] == 'color') {
          $element[$child]['#type'] = 'textfield';
          $element[$child]['#attributes']['data-color-picker'] = 'data-color-picker';
          $element[$child]['#attached']['library'][] = 'image_properties/minicolors';
        }
      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $result = parent::massageFormValues($values, $form, $form_state);

    // Let every property plugin massage its values.
    /** @var \Drupal\image_properties\Plugin\ImagePropertyInterface $property */
    foreach ($this->propertyManager->getAll() as $property) {
      $result = $property->massageFormValues($values, $form, $form_state);
    }

    return $result;
  }

}
