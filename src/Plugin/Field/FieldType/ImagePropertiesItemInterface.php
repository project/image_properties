<?php

namespace Drupal\image_properties\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Image\ImageInterface;

/**
 * Interface for the image properties field item.
 */
interface ImagePropertiesItemInterface extends FieldItemInterface {

  /**
   * Retrieve the image property manager.
   *
   * @return \Drupal\image_properties\Plugin\ImagePropertyManagerInterface
   *   The image property plugin manager.
   */
  public function getPropertyManager();

  /**
   * Load the image from the configured image field.
   *
   * @return \Drupal\Core\Image\ImageInterface|null
   *   The image contained in the configured image field.
   */
  public function loadReferenceImage();

  /**
   * Compute the field value.
   *
   * @param \Drupal\Core\Image\ImageInterface|null $image
   *   The reference image. If no image is provided it is obtained through
   *   static::loadReferenceImage().
   *
   * @return array
   *   The computed field values. Properties that can not be computed fall back
   *   to the current value of this field item.
   */
  public function computeValue(ImageInterface $image = NULL);

}
