<?php

namespace Drupal\image_properties\Plugin\Field\FieldType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\field_ui\Form\FieldConfigEditForm;
use Drupal\file\FileInterface;
use Drupal\image_properties\Plugin\ImagePropertyInterface;

/**
 * Plugin implementation of the 'image_properties' field type.
 *
 * @FieldType(
 *   id = "image_properties",
 *   label = @Translation("Image properties"),
 *   description = @Translation("This field stores advanced image properties."),
 *   default_widget = "image_properties_default",
 * )
 */
class ImagePropertiesItem extends FieldItemBase implements ImagePropertiesItemInterface {

  /**
   * The image property manager.
   *
   * @var \Drupal\image_properties\Plugin\ImagePropertyManagerInterface
   */
  protected $propertyManager;

  /**
   * {@inheritdoc}
   */
  public function getPropertyManager() {
    if (!$this->propertyManager) {
      $this->propertyManager = \Drupal::service('plugin.manager.image_property');
    }
    return $this->propertyManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field) {
    $schema = [
      'columns' => [
        'compute' => [
          'type' => 'int',
          'size' => 'tiny',
          'not null' => TRUE,
          'default' => 1,
        ],
      ],
      'indexes' => [],
      'foreign keys' => [],
    ];

    // Merge with schema from all plugins.
    /** @var \Drupal\image_properties\Plugin\ImagePropertyManagerInterface $property_manager */
    $property_manager = \Drupal::service('plugin.manager.image_property');
    /** @var ImagePropertyInterface $property */
    foreach ($property_manager->getAll() as $property) {
      $property_schema = $property->schema();
      if (isset($property_schema['columns'])) {
        $schema['columns'] += $property_schema['columns'];
      }
      if (isset($property_schema['indexes'])) {
        $schema['indexes'] += $property_schema['indexes'];
      }
      if (isset($property_schema['foreign keys'])) {
        $schema['foreign keys'] += $property_schema['foreign keys'];
      }
    }

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['compute'] = DataDefinition::create('boolean')
      ->setLabel(t('Compute value'));

    // Merge with property definitions from all plugins.
    /** @var \Drupal\image_properties\Plugin\ImagePropertyManagerInterface $property_manager */
    $property_manager = \Drupal::service('plugin.manager.image_property');
    /** @var ImagePropertyInterface $property */
    foreach ($property_manager->getAll() as $property) {
      $properties += $property->propertyDefinitions($field_definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'image_field' => NULL,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    $settings = $this->getSettings();

    /** @var FieldConfigEditForm $field_config_form */
    $field_config_form = $form_state->getFormObject();
    if (!$field_config_form instanceof FieldConfigEditForm) {
      return $element;
    }

    /** @var \Drupal\Core\Field\FieldConfigInterface $field_config */
    $field_config = $field_config_form->getEntity();
    $entity_type_id = $field_config->getTargetEntityTypeId();
    $entity_bundle = $field_config->getTargetBundle();

    // Collect all image fields on the entity type.
    $image_field_options = [NULL => '- None -'];
    $default_image_field = isset($settings['image_field']) ? $settings['image_field'] : NULL;
    /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager */
    $entity_field_manager = \Drupal::service('entity_field.manager');
    foreach ($entity_field_manager->getFieldDefinitions($entity_type_id, $entity_bundle) as $field_name => $field_definition) {
      if ($field_definition->getType() != 'image') {
        continue;
      }
      if ($default_image_field === NULL) {
        $default_image_field = $field_name;
      }
      $image_field_options[$field_name] = $field_definition->getName();
    }

    // Field to select the reference image field.
    $element['image_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Image field'),
      '#description' => $this->t('Select an image field for which the value of this field should be auto-filled when the entity is saved.'),
      '#options' => $image_field_options,
      '#default_value' => $default_image_field,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->values);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    if ($this->compute) {
      $this->setValue($this->computeValue());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function applyDefaultValue($notify = TRUE) {
    parent::applyDefaultValue($notify);

    // @todo Not sure if it should be removed.
    $this->setValue(['compute' => 1] + $this->getValue(), $notify);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function loadReferenceImage() {
    $entity = $this->getEntity();
    $image_field = $this->getSetting('image_field');

    if ($image_field && $entity->hasField($image_field) && $entity->getFieldDefinition($image_field)->getType() == 'image') {
      /** @var FileInterface|NULL $file */
      $file = $entity->get($image_field)->entity;
      if (!$file instanceof FileInterface) {
        return NULL;
      }

      /** @var \Drupal\Core\Image\ImageFactory $image_factory */
      $image_factory = \Drupal::service('image.factory');
      $image = $image_factory->get($file->getFileUri());

      return $image;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function computeValue(ImageInterface $image = NULL) {
    $values = $this->values;

    $image = $image ? $image : $this->loadReferenceImage();
    if (!$image instanceof ImageInterface) {
      return $values;
    }

    /** @var ImagePropertyInterface $property */
    $all_properties = $this->getPropertyManager()->getAll();
    $properties_sorted = $this->buildPropertyDependency(iterator_to_array($all_properties->getIterator()));
    foreach ($properties_sorted as $property) {
      $values = $property->computeValue($this, $values, $image) + $values;
    }

    return $values;
  }

  /**
   * Sort properties by dependencies.
   *
   * Currently there is no check for circular dependencies, that seems to be
   * quite complicated. @todo Review circular dependency checking options.
   *
   * @param ImagePropertyInterface[] $properties
   *   Property plugins keyed by plugin ID.
   *
   * @return ImagePropertyInterface[]
   *   Property plugins keyed by plugin ID, sorted by dependencies.
   */
  protected function buildPropertyDependency(array $properties) {
    $result = $properties;

    uasort($result, function (ImagePropertyInterface $a, ImagePropertyInterface $b) {
      $dep_a = $a->getPropertyDependencies();
      $dep_b = $b->getPropertyDependencies();
      if (in_array($a->getPluginId(), $dep_b)) {
        return -1;
      }
      elseif (in_array($b->getPluginId(), $dep_a)) {
        return 1;
      }
      else {
        return 0;
      }
    });

    return $result;
  }

}
