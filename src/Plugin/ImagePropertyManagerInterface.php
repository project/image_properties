<?php

namespace Drupal\image_properties\Plugin;

use Drupal\Component\Plugin\Discovery\CachedDiscoveryInterface;
use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface for the image property plugin manager.
 */
interface ImagePropertyManagerInterface extends PluginManagerInterface, CachedDiscoveryInterface {

  /**
   * Get all image property plugins.
   *
   * @return \Drupal\Component\Plugin\LazyPluginCollection
   *   Plugin collection containing all image property plugins.
   */
  public function getAll();

}
