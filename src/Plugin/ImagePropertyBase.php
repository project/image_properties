<?php

namespace Drupal\image_properties\Plugin;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\image_properties\Plugin\Field\FieldType\ImagePropertiesItemInterface;

/**
 * Base class for image property plugins with default method implementations.
 */
class ImagePropertyBase extends PluginBase implements ImagePropertyInterface {

  /**
   * {@inheritdoc}
   */
  public function getProvider() {
    return $this->pluginDefinition['provider'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->pluginDefinition['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function schema() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyDependencies() {
    return $this->pluginDefinition['derived_from'];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(ImagePropertiesItemInterface $item, $delta, array &$element, FormStateInterface $form_state) {
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function computeValue(ImagePropertiesItemInterface $item, array $values, ImageInterface $image) {
    return [];
  }

}
