<?php

namespace Drupal\image_properties\Plugin\ImageProperties\Property;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\image_properties\ImageProcessingInterface;
use Drupal\image_properties\Plugin\Field\FieldType\ImagePropertiesItemInterface;
use Drupal\image_properties\Plugin\ImagePropertyBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'average_color' image property.
 *
 * @ImageProperty(
 *   id = "average_color",
 *   label = "Color tone",
 *   description = "The average color tone of an image."
 * )
 */
class AverageColor extends ImagePropertyBase implements ContainerFactoryPluginInterface {

  /**
   * The image processing service.
   *
   * @var \Drupal\image_properties\ImageProcessingInterface
   */
  protected $imageProcessing;

  /**
   * AverageColor constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\image_properties\ImageProcessingInterface $image_processing
   *   The image processing service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ImageProcessingInterface $image_processing) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->imageProcessing = $image_processing;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('image_properties.image_processing')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function schema() {
    return [
      'columns' => [
        'average_color' => [
          'type' => 'varchar',
          'length' => 32,
          'default' => '',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['average_color'] = DataDefinition::create('string')
      ->setLabel($this->t('Average color'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(ImagePropertiesItemInterface $item, $delta, array &$element, FormStateInterface $form_state) {
    $element['average_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Color tone'),
      '#description' => $this->t('The overall tone of the image.'),
      '#default_value' => $item->average_color,
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    return parent::massageFormValues($values, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function computeValue(ImagePropertiesItemInterface $item, array $values, ImageInterface $image) {
    return [
      'average_color' => $this->imageProcessing->computeAverageImageColor($image),
    ];
  }

}
