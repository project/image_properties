<?php

namespace Drupal\image_properties\Plugin\ImageProperties\Property;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\image_properties\ImageProcessingInterface;
use Drupal\image_properties\Plugin\Field\FieldType\ImagePropertiesItemInterface;
use Drupal\image_properties\Plugin\ImagePropertyBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'luminosity' image property.
 *
 * @ImageProperty(
 *   id = "luminosity",
 *   label = "Luminosity",
 *   description = "The overall luminosity of an image.",
 *   derived_from = {"average_color"}
 * )
 */
class Luminosity extends ImagePropertyBase implements ContainerFactoryPluginInterface {

  /**
   * The image processing service.
   *
   * @var \Drupal\image_properties\ImageProcessingInterface
   */
  protected $imageProcessing;

  /**
   * Luminosity constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\image_properties\ImageProcessingInterface $image_processing
   *   The image processing service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ImageProcessingInterface $image_processing) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->imageProcessing = $image_processing;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('image_properties.image_processing')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function schema() {
    return [
      'columns' => [
        'luminosity' => [
          'type' => 'numeric',
          'precision' => 12,
          'scale' => 11,
          'default' => NULL,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['luminosity'] = DataDefinition::create('float')
      ->setLabel($this->t('Luminosity'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(ImagePropertiesItemInterface $item, $delta, array &$element, FormStateInterface $form_state) {
    $element['luminosity'] = [
      '#type' => 'range',
      '#title' => $this->t('Luminosity'),
      '#description' => $this->t('The overall luminosity of the image.'),
      '#min' => 0,
      '#max' => 1,
      '#step' => 'any',
      '#default_value' => $item->luminosity,
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $result = parent::massageFormValues($values, $form, $form_state);

    // Ensure the luminosity to be float, otherwise the form does not validate
    // when the value is empty (which means "" empty string).
    foreach (array_keys($result) as $delta) {
      if (!is_numeric($result[$delta]['luminosity'])) {
        $result[$delta]['luminosity'] = 0.5;
      }
      else {
        $result[$delta]['luminosity'] = (float) $result[$delta]['luminosity'];
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function computeValue(ImagePropertiesItemInterface $item, array $values, ImageInterface $image) {
    return [
      'luminosity' => $this->imageProcessing->computeColorLuminosity($values['average_color']),
    ];
  }

}
