<?php

namespace Drupal\image_properties\Annotation;

use Drupal\Component\Annotation\Plugin as PluginAnnotation;

/**
 * The annotation required for image property plugins.
 *
 * @Annotation
 */
class ImageProperty extends PluginAnnotation {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin label.
   *
   * @var string
   */
  public $label;

  /**
   * The plugin description.
   *
   * @var string
   */
  public $description;

  /**
   * Array of image properties this property depends on.
   *
   * @var array
   */
  public $derived_from = [];

}
