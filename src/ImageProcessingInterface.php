<?php

namespace Drupal\image_properties;

use Drupal\Core\Image\ImageInterface;

/**
 * Interface for the image processing service.
 *
 * This service bundles image processing related, reusable functions used by the
 * included image property plugins.
 */
interface ImageProcessingInterface {

  /**
   * Compute the average color of an image.
   *
   * @param \Drupal\Core\Image\ImageInterface $image
   *   The image.
   *
   * @return string|null
   *   Hex color string starting with #, or NULL if the calculation is not
   *   possible.
   */
  public function computeAverageImageColor(ImageInterface $image);

  /**
   * Compute the luminosity of a color.
   *
   * @param string $color
   *   The color as hex string.
   *
   * @return float
   *   The luminosity value in the range of 0.0 to 1.0.
   */
  public function computeColorLuminosity($color);

  /**
   * Helper function to convert hex string to an array of red, green and blue values.
   *
   * @param string $hex
   *   The hexadecimal string representation of the color.
   *
   * @return array
   *   The R,G,B values.
   */
  public function hexToRgb($hex);

  /**
   * Helper function to convert an array of red, green and blue values to hex string representation.
   *
   * @param array $rgb
   *   The R,G,B values.
   *
   * @return string
   *   The hexadecimal string representation of the color.
   */
  public function rgbToHex(array $rgb);

}
