# Image Properties Drupal 8 module

## Description

This module aims to provide advanced properties on images that can be computed
automatically. Properties are stored in a field which is composed of plugins
defining individual properties and declaring a method to compute the value from
an image or from other already computed properties (luminosity can be computed
from average color, saved MEM and CPU). The source image is taken from an image
field within the same entity, which can be selected on the field config form.

Initially the following properties are provided:

- Color tone (average color)
- Luminosity (average of average color RGB components)

The field values can be used by other modules or themes, for example a theme
could use the luminosity property to control the color of a text overlay on top
of an image (original motivation to create this module).
A field formatter does not exist yet, it might not be totally necessary to have
one.

Currently it includes the option to show color widgets with jQuery MiniColors
(included with composer) or the build-in 'color' field type. This was an
experimental move, integration with
https://www.drupal.org/project/jquery_minicolors would be better, probably.

## Installation

The usual way, with Drupal Console, drush or through the backend.

## Configuration

  * Add an "Image properties" field to an entity with at least one image field.
  * On the field configuration form select the image field you wish to
  auto-generate the image properties for.
  * Configure the field widget on the Manage form display page.
